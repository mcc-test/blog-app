﻿using Blog.DTO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog
{
    public class BlogService
    {
        private readonly MyDbContext _context;

        public BlogService(MyDbContext context)
        {
            _context = context;
        }

        public IEnumerable<UserCommentsAmount> NumberOfCommentsPerUser()
        {
            return _context.BlogComments
                .GroupBy(x => x.UserName)
                .Select(comments =>
                    new UserCommentsAmount
                    {
                        Name = comments.Key,
                        Amount = comments.Count(),
                    }
                )
                .OrderByDescending(x => x.Amount);
        }
        
        public IEnumerable<PostWithLastComment> PostsOrderedByLastCommentDate()
        {
            return _context.BlogPosts
                .Include(x => x.Comments)
                .Select(x => new PostWithLastComment
                {
                    Title = x.Title,
                    Date = x.Comments.Max(comment => comment.CreatedDate),
                    Text = x.Comments.OrderByDescending(comment => comment.CreatedDate).First().Text,
                })
                .OrderByDescending(x => x.Date);
        }

        public IEnumerable<UserCommentsAmount> NumberOfLastCommentsLeftByUser()
        {
            return _context.BlogPosts
                .Include(x => x.Comments)
                .Select(x => x.Comments.OrderByDescending(y => y.CreatedDate).First())
                .GroupBy(x => x.UserName)
                .Select(x => new UserCommentsAmount
                {
                    Name = x.Key,
                    Amount = x.Count(),
                });
        }
    }
}
