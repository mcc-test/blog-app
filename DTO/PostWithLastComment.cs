﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.DTO
{
    public class PostWithLastComment
    {
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
}
