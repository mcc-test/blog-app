﻿namespace Blog.DTO
{
    public class UserCommentsAmount
    {
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}
